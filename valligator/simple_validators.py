"""Simple one-function validators."""
import logging
import re


def contains_keyval(cover, patches, key):
    """Check if a 'key: <value>' is present."""
    pattern = re.compile(r'^{}:\s+\S+$'.format(key), re.I | re.M)

    if pattern.search(cover):
        return True

    to_return = True
    for index, patch in enumerate(patches):
        if not pattern.search(patch):
            logging.error('Patch %d doesn\'t contain %s!', index + 1, key)
            to_return = False

    return to_return


def subject_contains_keyword(cover, patches, key):
    """Check the subject contains a required keyword"""
    to_return = True

    pattern = re.compile(r'^Subject:\s.*\[.*{}.*\]'.format(key),
                         re.M)

    if cover and not pattern.search(cover):
        logging.error('Cover letter doesn\'t contain %s!', key)
        to_return = False

    for index, patch in enumerate(patches):
        if not pattern.search(patch):
            to_return = False
            logging.error('Patch %d doesn\'t contain %s!', index + 1, key)

    return to_return


def must_have_cover(cover, patches, must):
    """
    Check the existence of a cover letter when required.
    """

    to_return = True
    must = must.lower()

    if must not in ["true", "false", "series"]:
        logging.error("Unrecognized argument: %s", must)
        return False

    if not cover:
        if must == "true":
            to_return = False
            logging.error("You need to provide with a cover letter.")
        if must == "series" and len(patches) > 1:
            to_return = False
            logging.error("%d patches provided. You need to attach the cover.",
                          len(patches))
    return to_return


def max_line_length(cover, patches, max_len):
    """
    Check the lines added in the patches aren't longer than X characters.
    """

    to_return = True
    max_len = int(max_len)

    pattern = re.compile(r'^\+(.*)', re.M)
    for npatch, patch in enumerate(patches):
        target_lines = pattern.findall(patch)
        for line in target_lines:
            if len(line) > max_len:
                to_return = False
                logging.error('Line [ %s ] is too long', line)

    return to_return


def generic_patch_dependency(cover, patches, key):
    """
    Check if a Depends: <tag> or similar field is present in the patch.
    If so, it usually means the patch depends on a different patch that
    is not merged yet and can't be tested properly without it.
    """
    to_return = True
    pattern_dependency = re.compile(
        r'^(Depends|Depends on|{}):\s+(.*)$'.format(key),
        re.I | re.M
    )

    for npatch, patch in enumerate(patches):
        dependencies = pattern_dependency.findall(patch)

        for dep in dependencies:
            logging.error("Patch %d %s", npatch + 1, dep)
            to_return = False

    return to_return


def all_files(cover, patches, key):
    """
    Check whether all files concerned are included.
    All patches of the serie must be available and the argument true
    """

    to_return = True
    npatches_need = 0
    key = key.lower()

    if not patches:
        logging.error("No patches provided")
        to_return = False

    elif key == "false":
        logging.error("all_files validator disabled")
        to_return = True

    elif key == "true":
        pattern = re.compile(r'^Subject:\s.*\[.*\/(\d+).*]', re.M)
        npatches = pattern.search(patches[0])
        if npatches:
            npatches_need = int(npatches.group(1))

        npatches_prov = len(patches)
        if npatches_need > npatches_prov:
            logging.error("%d patches provided out of the total %d desired.",
                          npatches_prov, npatches_need)
            to_return = False

    else:
        logging.error("Wrong argument provided for all_files validator")
        to_return = False

    return to_return
